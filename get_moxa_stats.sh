#!/bin/bash
file="moxa-snmp-$(date +%Y%m%d_%H%M%S).csv"
echo "Signal,RSSI,Noise,SNRA,SNRB" > $file
IP_ADDR="11.0.0.1"
while true
do
	#rssxi=$(snmpwalk -Oqv -v1 -c public $IP_ADDR MOXA-AWK1137C-MIB::wirelessStatusRSSI)
	rssxi=$(snmpwalk -Oqv -v1 -c public $IP_ADDR .1.3.6.1.4.1.8691.15.35.1.9.5.7.0)
	noise=$(snmpwalk -Oqv -v1 -c public $IP_ADDR .1.3.6.1.4.1.8691.15.35.1.11.17.1.12)
	snra=$(snmpwalk -Oqv -v1 -c public $IP_ADDR .1.3.6.1.4.1.8691.15.35.1.11.17.1.13)
	snrb=$(snmpwalk -Oqv -v1 -c public $IP_ADDR .1.3.6.1.4.1.8691.15.35.1.11.17.1.14)
	date=$(date +%s)
	echo "$date, $rssxi, $noise" >> $file
	echo "Date: $date R: $rssxi n: $noise"
	sleep 1
done
